package com.example.demo.property.domain.repository;

import com.example.demo.property.domain.model.Property;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by snowwhite on 6/16/2017.
 */
@Repository
public interface CustomPropertyRepository {
	List<Property> findAvailable(String cityName, LocalDate startDate, LocalDate endDate);
	boolean isPropertyAvailable(String id, LocalDate startDate, LocalDate endDate);
}
