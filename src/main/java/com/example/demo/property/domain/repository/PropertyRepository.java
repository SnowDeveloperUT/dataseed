package com.example.demo.property.domain.repository;

import com.example.demo.property.domain.model.Property;
import com.oracle.xmlns.internal.webservices.jaxws_databinding.JavaMethod;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/16/2017.
 */
@Repository
public interface PropertyRepository extends JpaRepository<Property,String>, CustomPropertyRepository{}
