package com.example.demo.property.domain.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/16/2017.
 */
@Entity
@Getter
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class Property {

	@Id
	String id;

	BigDecimal price;

	String address;

	int area;

	int floor;

	String amenities;

	String services;

}
