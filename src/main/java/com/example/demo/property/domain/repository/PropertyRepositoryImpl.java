package com.example.demo.property.domain.repository;

import com.example.demo.property.domain.model.Property;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by snowwhite on 6/16/2017.
 */
public class PropertyRepositoryImpl implements CustomPropertyRepository{

	@Autowired
	EntityManager em;


	@Override
	public List<Property> findAvailable(String cityName, LocalDate startDate, LocalDate endDate) {
		return em.createQuery(
				"select i from Property i where lower(i.address) like ?1 and i not in (select r.property from Booking r where r.bookingStatus = 'ACCEPTED' and ?2 < r.bookingDates.endDate and ?3 > r.bookingDates.startDate)"
				, Property.class)
				.setParameter(1, "%" + cityName.toLowerCase()+ "%")
				.setParameter(2, startDate)
				.setParameter(3, endDate)
				.getResultList();
	}

	@Override
	public boolean isPropertyAvailable(String id, LocalDate startDate, LocalDate endDate) {
		return em.createQuery("select case when count(*) > 0 then true else false end " +
						"from Property i where i.id = ?1 and i not in (select r.property from Booking r where r.bookingStatus = 'ACCEPTED' and ?2 < r.bookingDates.endDate and ?3 > r.bookingDates.startDate)"
				, Boolean.class)
				.setParameter(1, id )
				.setParameter(2, startDate)
				.setParameter(3, endDate)
				.getSingleResult();
	}
}
