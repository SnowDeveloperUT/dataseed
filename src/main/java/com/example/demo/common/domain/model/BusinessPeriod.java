package com.example.demo.common.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.persistence.Embeddable;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * Created by snowwhite on 6/16/2017.
 */
@Embeddable
@Value
@NoArgsConstructor(force=true,access= AccessLevel.PRIVATE)
@AllArgsConstructor(staticName="of")
public class BusinessPeriod {

	LocalDate startDate;
	LocalDate endDate;

	public long numberOfDays() {
		return ChronoUnit.DAYS.between(startDate, endDate) + 1;
	}

}
