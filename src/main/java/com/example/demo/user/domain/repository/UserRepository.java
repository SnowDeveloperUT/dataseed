package com.example.demo.user.domain.repository;

import com.example.demo.user.domain.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/16/2017.
 */
@Repository
public interface UserRepository extends JpaRepository<User,String>{}
