package com.example.demo.user.domain.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

/**
 * Created by snowwhite on 6/16/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class User {

	@Id
	String id;

	String name;

	BigDecimal credit;

	public void setCredit(BigDecimal credit) {
		this.credit = credit;
	}

}
