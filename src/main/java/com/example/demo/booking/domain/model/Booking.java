package com.example.demo.booking.domain.model;

import com.example.demo.common.domain.model.BusinessPeriod;
import com.example.demo.property.domain.model.Property;
import com.example.demo.user.domain.model.User;
import com.oracle.webservices.internal.api.message.PropertySet;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/**
 * Created by snowwhite on 6/16/2017.
 */
@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class Booking {

	@Id
	String id;

	@ManyToOne
	User user;

	@ManyToOne
	Property property;

	@Embedded
	BusinessPeriod bookingDates;

	@Enumerated(EnumType.STRING)
	BookingStatus bookingStatus;

	public void setBookingStatus(BookingStatus bookingStatus) {
		this.bookingStatus = bookingStatus;
	}

}
