package com.example.demo.booking.domain.model;

/**
 * Created by snowwhite on 6/16/2017.
 */
public enum BookingStatus {

	ACCEPTED, CANCELLED, CLOSED
}
