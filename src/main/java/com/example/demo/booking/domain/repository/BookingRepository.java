package com.example.demo.booking.domain.repository;

import com.example.demo.booking.domain.model.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by snowwhite on 6/16/2017.
 */
@Repository
public interface BookingRepository extends JpaRepository<Booking, String>{}
