insert into property (id, price, address, area, floor, amenities, services) values (101, 90, 'Tartu', 20, 2, 'Heating, Electricity etc','Kitchen, Bathroom etc');
insert into property (id, price, address, area, floor, amenities, services) values (102, 190, 'Tartu', 20, 2, 'Heating, Electricity etc','Kitchen, Bathroom etc');
insert into property (id, price, address, area, floor, amenities, services) values (103, 60, 'Tartu', 20, 3, 'Heating, Electricity etc','Kitchen, Bathroom etc');
insert into property (id, price, address, area, floor, amenities, services) values (104, 70, 'Tallin', 20, 2, 'Heating, Electricity etc','Kitchen, Bathroom etc');
insert into property (id, price, address, area, floor, amenities, services) values (105, 80, 'Tallin', 20, 7, 'Heating, Electricity etc','Kitchen, Bathroom etc');
insert into property (id, price, address, area, floor, amenities, services) values (106, 30, 'Tallin', 20, 1, 'Heating, Electricity etc','Kitchen, Bathroom etc');
insert into property (id, price, address, area, floor, amenities, services) values (107, 45, 'Tallin', 20, 2, 'Heating, Electricity etc','Kitchen, Bathroom etc');


insert into user (id, name, credit) values (1, 'Mark', 50);
insert into user (id, name, credit) values (2, 'Peter', 100);

insert into booking (id, user_id, property_id, start_date, end_date, booking_status) values (100, 1, 101, '2017-06-10', '2017-06-12', 'ACCEPTED');
insert into booking (id, user_id, property_id, start_date, end_date, booking_status) values (101, 1, 101, '2017-07-01', '2017-07-07', 'ACCEPTED');
insert into booking (id, user_id, property_id, start_date, end_date, booking_status) values (102, 1, 102, '2017-07-03', '2017-07-05', 'CANCELLED');
insert into booking (id, user_id, property_id, start_date, end_date, booking_status) values (103, 1, 103, '2017-07-02', '2017-07-04', 'CLOSED');
